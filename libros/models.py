from django.db import models


class Autor(models.Model):
    nombre = models.CharField(
        max_length=30
    )
    nacionalidad = models.CharField(
        max_length=30
    )

    class Meta:
        verbose_name_plural = 'Autores'


class Libro(models.Model):
    autor = models.ManyToManyField(
        Autor,
        blank=True
    )
    descripcion = models.CharField(
        verbose_name='Descripción',
        max_length=50
    )
    cantidad = models.PositiveSmallIntegerField()
    edicion = models.PositiveSmallIntegerField()
    observacion = models.TextField()
