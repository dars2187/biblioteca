from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from libros.models import (
    Autor,
    Libro
)


@admin.register(Autor)
class AutorAdmin(ImportExportActionModelAdmin):
    list_display = [
        'id',
        'nombre',
        'nacionalidad'
    ]
    fields = (
        'nombre',
        'nacionalidad'
    )
    list_per_page = 5


@admin.register(Libro)
class LibroAdmin(ImportExportActionModelAdmin):
    list_display = [
        'id',
        'descripcion',
        'cantidad',
        'edicion',
        'observacion'
    ]
    fields = (
        'descripcion',
        'cantidad',
        'edicion',
        'observacion'
    )
    list_per_page = 5
