from django.conf.urls import url
from django.urls import reverse_lazy

from prestamos.views import (
    PrestamoList,
    PrestamoLibroList
)

urlpatterns = [
    url(
        r'^$',
        PrestamoList.as_view(),
        name='index'
    ),
    url(
        r'^Prestamo/(?P<pk>\d+)$',
        PrestamoLibroList.as_view(),
        name='detail'
    ),
]
