from django.shortcuts import render
from django.views.generic import ListView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView

from prestamos.models import (
    Prestamo,
    PrestamoLibro
)


class PrestamoList(LoginRequiredMixin, ListView):
    model = Prestamo
    context_object_name = 'prestamos'
    template_name = 'prestamos/index.html'


class PrestamoLibroList(LoginRequiredMixin, DetailView):
    model = PrestamoLibro
