from django.db import models
from django.contrib.auth.models import User

from libros.models import Libro


class Prestamo(models.Model):
    cliente = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    fecha = models.DateTimeField(
        auto_now_add=True
    )
    observacion = models.TextField()


class PrestamoLibro(models.Model):
    ESTADOS = (
        (1, 'En prestamo'),
        (2, 'Entregado'),
        (3, 'Vencido'),
    )
    libro = models.ForeignKey(
        Libro,
        on_delete=models.CASCADE
    )
    prestamo = models.ForeignKey(
        Prestamo,
        on_delete=models.CASCADE
    )
    estado = models.PositiveSmallIntegerField(
        choices=ESTADOS,
        null=True,
        blank=True
    )
    observacion = models.TextField()
