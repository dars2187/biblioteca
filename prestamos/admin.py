from django.contrib import admin


from prestamos.models import (
    Prestamo,
    PrestamoLibro
)


@admin.register(Prestamo)
class PrestamoAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'cliente',
        'fecha',
        'observacion'
    ]


@admin.register(PrestamoLibro)
class PrestamoLibroAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'libro',
        'prestamo',
        'estado',
        'observacion'
    ]
