from django.conf.urls import url
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.contrib.auth.models import User

from usuarios.views import RegistroUsuario


urlpatterns = [
    url(
        r'^login/$',
        LoginView.as_view(
            template_name='usuarios/login.html'
        ),
        name='login'
    ),
    url(
        r'^registrar/$',
        RegistroUsuario.as_view(),
        name='registrar'
    ),
    url(
        r'^logout/$',
        LogoutView.as_view(
            next_page=reverse_lazy('login')
        ),
        name='logout'
    ),
]
