from django.contrib import admin


from usuarios.models import Perfil


@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'usuario',
        'rol'
    ]
