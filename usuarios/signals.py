from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User

from usuarios.models import Perfil


@receiver(post_save, sender=User)
def asignar_perfil(sender, **kwargs):
    if kwargs.get('created', False):
        Perfil.objects.get_or_create(usuario=kwargs.get('instance'))
