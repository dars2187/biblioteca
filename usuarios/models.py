from django.db import models
from django.contrib.auth.models import User


class Perfil(models.Model):
    ROLE_CHOICES = (
        (1, 'Cliente'),
        (2, 'Bibliotecario'),
        (3, 'Administrador'),
    )
    usuario = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )
    rol = models.PositiveSmallIntegerField(
        choices=ROLE_CHOICES,
        null=True,
        blank=True,
        default=1
    )

    class Meta:
        verbose_name_plural = 'Perfiles'
