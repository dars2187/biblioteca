from django.conf.urls import (
    url,
    include
)
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(
        r'^usuarios/',
        include('usuarios.urls')
    ),
    url(
        r'^prestamos/',
        include('prestamos.urls')
    ),
    # url(
    #     r'^libros/',
    #     include('libros.urls')
    # ),
]
